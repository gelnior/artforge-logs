package fr.hd3d.logs.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.logs.ui.client.view.LogsView;


/**
 * Entry point classes for Logs application. It sets up the main view, and launch START event.
 */
public class Logs implements EntryPoint
{
    /**
     * This is the entry point method.
     */
    public void onModuleLoad()
    {
        LogsView logs = new LogsView();
        logs.init();

        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
