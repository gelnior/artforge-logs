package fr.hd3d.logs.ui.client.controller.context;

public interface ContextCommand
{

    public void execute(String value);
}
