package fr.hd3d.logs.ui.client.controller.context;

import java.util.Date;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.logs.ui.client.view.ILogsView;


public class EndDateAction implements ContextCommand
{
    private final ILogsView view;

    public EndDateAction(ILogsView view)
    {
        this.view = view;
    }

    public void execute(String value)
    {
        try
        {
            this.view.setEndDate(new Date(Long.valueOf(value)));
        }
        catch (Exception e)
        {
            this.view.setEndDate(DatetimeUtil.tomorrow());
        }
    }

}
