package fr.hd3d.logs.ui.client.controller.context;

import java.util.Date;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.logs.ui.client.view.ILogsView;


public class StartDateAction implements ContextCommand
{
    private final ILogsView view;

    public StartDateAction(ILogsView view)
    {
        this.view = view;
    }

    public void execute(String value)
    {
        try
        {
            Date d = new Date(Long.valueOf(value));
            this.view.setStartDate(d);
        }
        catch (Exception e)
        {
            this.view.setStartDate(DatetimeUtil.today());
        }
    }
}
