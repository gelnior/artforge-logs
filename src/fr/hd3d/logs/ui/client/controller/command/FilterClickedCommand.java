package fr.hd3d.logs.ui.client.controller.command;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.history.HistoryUtils;
import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;
import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.logs.ui.client.model.LogsModel;
import fr.hd3d.logs.ui.client.model.modeldata.ConstraintForm;
import fr.hd3d.logs.ui.client.view.ILogsView;


public class FilterClickedCommand implements BaseCommand
{
    private final LogsModel model;
    private final ILogsView view;

    public FilterClickedCommand(LogsModel model, ILogsView view)
    {
        this.model = model;
        this.view = view;
    }

    private Long startDate = 0L;
    private Long endDate = 0L;

    public void setHistoryToken()
    {
        HistoryUtils.putValue("startdate", this.startDate.toString());
        HistoryUtils.putValue("enddate", this.endDate.toString());
    }

    /**
     * When "apply filter" is clicked, it sets the filter made by the user to the grid list then refreshes data.
     * 
     * @param event
     */
    public void execute(AppEvent event)
    {
        ConstraintForm form = this.model.getConstraintForm();

        this.startDate = this.view.convertDateToLong(form.getStartDate(), form.getStartHour());
        this.endDate = this.view.convertDateToLong(form.getEndDate(), form.getEndHour());
        this.setHistoryToken();

        Constraint constraint = new Constraint(EConstraintOperator.btw, AuditModelData.DATE_FIELD + "Ms", startDate,
                endDate);
        List<IUrlParameter> constraints = new ArrayList<IUrlParameter>();
        constraints.add(constraint);

        this.model.refreshData(constraints);
    }

}
