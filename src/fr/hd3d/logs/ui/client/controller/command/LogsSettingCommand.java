package fr.hd3d.logs.ui.client.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.logs.ui.client.controller.context.EndDateAction;
import fr.hd3d.logs.ui.client.controller.context.LogsContext;
import fr.hd3d.logs.ui.client.controller.context.StartDateAction;
import fr.hd3d.logs.ui.client.event.LogsEvent;
import fr.hd3d.logs.ui.client.model.LogsModel;
import fr.hd3d.logs.ui.client.view.ILogsView;


public class LogsSettingCommand implements BaseCommand
{
    private final LogsModel model;
    private final ILogsView view;

    public LogsSettingCommand(LogsModel model, ILogsView view)
    {
        this.model = model;
        this.view = view;
    }

    public void execute(AppEvent event)
    {
        this.model.initProxyPath(ServicesPath.AUDITS);
        this.model.initPagingProxy();

        this.view.initWidgets();
        this.view.hideStartPanel();
        this.view.autoSelectFilterFields();

        LogsContext.put("startdate", new StartDateAction(view));
        LogsContext.put("enddate", new EndDateAction(view));
        LogsContext.restoreContext();
        EventDispatcher.forwardEvent(LogsEvent.FILTER_CLICKED);
    }

}
