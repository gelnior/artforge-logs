package fr.hd3d.logs.ui.client.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.logs.ui.client.view.ILogsView;


public class LogsErrorCommand implements BaseCommand
{
    private final ILogsView view;

    public LogsErrorCommand(ILogsView view)
    {
        this.view = view;
    }

    public void execute(AppEvent event)
    {
        this.view.unmaskGrid();
    }
}
