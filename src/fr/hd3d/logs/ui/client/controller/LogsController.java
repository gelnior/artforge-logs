package fr.hd3d.logs.ui.client.controller;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.logs.ui.client.controller.command.FilterClickedCommand;
import fr.hd3d.logs.ui.client.controller.command.LogsErrorCommand;
import fr.hd3d.logs.ui.client.controller.command.LogsSettingCommand;
import fr.hd3d.logs.ui.client.event.LogsEvent;
import fr.hd3d.logs.ui.client.model.LogsModel;
import fr.hd3d.logs.ui.client.view.ILogsView;


/**
 * Main controller to handle configuration, error and start events.
 * 
 * @author HD3D
 */
public class LogsController extends NewMainController
{
    /** Default constructor. */
    public LogsController(LogsModel model, ILogsView view)
    {
        super(model, view);

        super.registerEvents();
        this.registerEvents(model, view);
    }

    /**
     * Register all events the controller can handle.
     * 
     * @param view
     * @param model
     */
    protected void registerEvents(LogsModel model, ILogsView view)
    {
        commandMap.put(CommonEvents.SETTINGS_INITIALIZED, new LogsSettingCommand(model, view));
        commandMap.put(LogsEvent.FILTER_CLICKED, new FilterClickedCommand(model, view));
        commandMap.put(CommonEvents.ERROR, new LogsErrorCommand(view));
    }
}
