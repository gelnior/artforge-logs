package fr.hd3d.logs.ui.client.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.Hd3dClassReadersFactory;
import fr.hd3d.common.ui.client.service.PermissionPath;
import fr.hd3d.common.ui.client.service.ServicesClass;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.userrights.UserRightsResolver;
import fr.hd3d.common.ui.client.widget.mainview.IMainView;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


public class NewMainController extends CommandController
{
    /** Logs widget view */
    final private IMainView mainView;
    /** Data model */
    final private MainModel mainModel;

    /** Default constructor. */
    public NewMainController(MainModel model, IMainView view)
    {
        this.mainModel = model;
        this.mainView = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();
        if (type == CommonEvents.START)
        {
            this.onStart();
        }
        else if (type == CommonEvents.CONFIG_INITIALIZED)
        {
            this.onConfigInitialized(event);
        }
        else if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionsInitialized(event);
        }
        else if (type == CommonEvents.PERSON_INITIALIZED)
        {
            this.onPersonInitialized(event);
        }
        else if (type == CommonEvents.SETTINGS_INITIALIZED)
        {
            this.onSettingInitialized(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
    }

    /**
     * When application starts, it retrieves configuration file and sets service URL into the REST request handler. <br>
     * Then it initializes:
     * <ul>
     * <li>Dictionary for bound class name / services path correspondence.</li>
     * <li>Dictionary for bound class name / services class correspondence.</li>
     * <li>Dictionary for human name field / JSON name field correspondence.</li>
     * <li>Dictionary for bound class name / model type correspondence.</li>
     * <li>Dictionary for bound class name / permission path correspondence.</li>
     * <li>List of excluded field for displaying.</li>
     * </ul>
     */
    protected void onStart()
    {
        this.mainView.showStartPanel();

        ServicesPath.initPath();
        ServicesClass.initClass();
        Hd3dClassReadersFactory.initClass();
        ServicesModelType.initModelTypes();

        ServicesField.initFields();
        ExcludedField.initExcludedFields();

        PermissionPath.initPath();

        this.mainModel.initServicesConfig();
    }

    /**
     * When configuration is initialized, it retrieves user rights.
     */
    protected void onConfigInitialized(AppEvent event)
    {
        UserRightsResolver.acquireUserRights();
    }

    /**
     * When user rights are initialized, it retrieves current user data.
     */
    protected void onPermissionsInitialized(AppEvent event)
    {
        this.mainModel.retrieveCurrentUser();
    }

    /**
     * When current user data are initialized, it retrieves current user settings.
     */
    private void onPersonInitialized(AppEvent event)
    {
        this.mainModel.retrieveSettings();
    }

    /**
     * When user rights are initialized, it does nothing.
     */
    protected void onSettingInitialized(AppEvent event)
    {}

    /**
     * When error occurs, it displays an appropriate message depending on the error code.
     * 
     * @param event
     *            The error event.
     */
    protected void onError(AppEvent event)
    {
        Integer error = event.getData(CommonConfig.ERROR_EVENT_VAR_NAME);
        String userMsg = event.getData(CommonConfig.ERROR_MSG_EVENT_VAR_NAME);
        String stack = event.getData(CommonConfig.ERROR_TECH_MSG_EVENT_VAR_NAME);

        this.mainView.hideStartPanel();
        if (error != null)
        {
            this.mainView.displayError(error.intValue(), userMsg, stack);
        }

        this.forwardToChild(event);
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        this.registerEventTypes(CommonEvents.START);
        this.registerEventTypes(CommonEvents.CONFIG_INITIALIZED);
        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        this.registerEventTypes(CommonEvents.PERSON_INITIALIZED);
        this.registerEventTypes(CommonEvents.SETTINGS_INITIALIZED);
        this.registerEventTypes(CommonEvents.ERROR);
    }
}
