package fr.hd3d.logs.ui.client.model.modeldata;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dPagingJsonReader;
import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;


/**
 * Specific JSON reader for audit data.
 * 
 * @author HD3D
 */
public class AuditPagingReader extends Hd3dPagingJsonReader<AuditModelData>
{
    public AuditPagingReader()
    {
        super(new AuditModelType());
    }

    @Override
    public AuditModelData newModelInstance()
    {
        return new AuditModelData();
    }
}
