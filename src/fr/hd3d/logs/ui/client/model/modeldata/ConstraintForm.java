package fr.hd3d.logs.ui.client.model.modeldata;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.form.Time;


/**
 * Constraint form contains value the user gives via UI for filtering the log grid data.<br>
 * Filter available : min date-time, max date-time.
 * 
 * @author HD3D
 */
public class ConstraintForm extends BaseModelData
{
    /** Automatically generated serial number */
    private static final long serialVersionUID = 4397326957731372339L;

    public static String END_HOUR_FIELD = "endHour";
    public static String START_HOUR_FIELD = "startHour";
    public static String END_DATE_FIELD = "endDate";
    public static String START_DATE_FIELD = "startDate";

    /**
     * @return The upper date limit of the date filter.
     */
    public Date getStartDate()
    {
        return this.get(START_DATE_FIELD);
    }

    public void setStartDate(Date date)
    {
        this.set(START_DATE_FIELD, date);
    }

    /**
     * @return The upper time limit of the date filter.
     */
    public Time getStartHour()
    {
        return this.get(START_HOUR_FIELD);
    }

    public void setStartHour(Time time)
    {
        this.set(START_HOUR_FIELD, time);
    }

    /**
     * @return The lower date limit of the date filter.
     */
    public Date getEndDate()
    {
        return this.get(END_DATE_FIELD);
    }

    public void setEndDate(Date date)
    {
        this.set(END_DATE_FIELD, date);
    }

    /**
     * @return The lower time limit of the date filter.
     */
    public Time getEndHour()
    {
        return this.get(END_HOUR_FIELD);
    }

    public void setEndHour(Time time)
    {
        this.set(END_HOUR_FIELD, time);
    }

    /**
     * @return The upper date-time limit of the date filter.
     */
    public Long getStartDateTime()
    {
        DateWrapper date = new DateWrapper(getStartDate());
        date = date.addHours(getStartHour().getHour());
        date = date.addMinutes(getStartHour().getMinutes());
        Date startDate = date.asDate();

        Long dateLong = new Long(startDate.getTime());

        return dateLong;
    }

    /**
     * @return The lower date-time limit of the date filter.
     */
    public Long getEndDateTime()
    {
        DateWrapper date = new DateWrapper(getEndDate());
        date = date.addHours(getEndHour().getHour());
        date = date.addMinutes(getEndHour().getMinutes());
        Date startDate = date.asDate();

        Long dateLong = new Long(startDate.getTime());

        return dateLong;
    }
}
