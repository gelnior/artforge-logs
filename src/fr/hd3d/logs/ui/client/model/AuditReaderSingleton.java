package fr.hd3d.logs.ui.client.model;

import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;
import fr.hd3d.logs.ui.client.model.modeldata.AuditPagingReader;


/**
 * Give audit reader instance needed to read JSON data from server.
 * 
 * @author HD3D
 */
public class AuditReaderSingleton
{
    /** The record reader instance to return. */
    private static IPagingReader<AuditModelData> reader;

    /**
     * @return The record reader instance. If it is null, it returns a new real record reader.
     */
    public final static IPagingReader<AuditModelData> getInstance()
    {
        if (reader == null)
        {
            reader = new AuditPagingReader();
        }
        return reader;
    }

    /**
     * Sets the record instance. This method is useful for unit tests.
     * 
     * @param mock
     *            The request handler to set.
     */
    public final static void setInstanceAsMock(IPagingReader<AuditModelData> mock)
    {
        reader = mock;
    }
}
