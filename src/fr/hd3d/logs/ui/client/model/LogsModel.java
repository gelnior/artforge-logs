package fr.hd3d.logs.ui.client.model;

import java.util.List;

import com.extjs.gxt.ui.client.data.BasePagingLoader;
import com.extjs.gxt.ui.client.data.PagingLoadResult;
import com.extjs.gxt.ui.client.data.PagingLoader;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.proxy.ServicesPagingProxy;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.logs.ui.client.config.LogsConfig;
import fr.hd3d.logs.ui.client.model.modeldata.ConstraintForm;


/**
 * Handles logs grid data.
 * 
 * @author HD3D
 */
public class LogsModel extends MainModel
{
    /** Log grid data store. */
    private ListStore<AuditModelData> store;
    /** Log grid data loader. */
    private PagingLoader<PagingLoadResult<AuditModelData>> loader;

    /** Form containing constraint to apply for filtering grid data. */
    private final ConstraintForm constraintForm = new ConstraintForm();
    /** Proxy needed by the loader to retrieve data from the web services. */
    private final ServicesPagingProxy<AuditModelData> restletProxy = new ServicesPagingProxy<AuditModelData>();

    /**
     * Default constructor.
     */
    public LogsModel()
    {}

    /**
     * @return Log grid data store.
     */
    public ListStore<AuditModelData> getStore()
    {
        return this.store;
    }

    /**
     * @return Log grid data loader.
     */
    public PagingLoader<?> getLoader()
    {
        return this.loader;
    }

    /**
     * @return Constraint form value (needed for grid filtering).
     */
    public ConstraintForm getConstraintForm()
    {
        return this.constraintForm;
    }

    /**
     * Load data from services into data store.
     */
    public void reloadStore()
    {
        if (this.loader != null)
        {
            this.loader.load(0, LogsConfig.PAGING_OFFSET);
        }
    }

    /**
     * Initialize the services path to use for the data proxy.
     * 
     * @param path
     *            Logs services path.
     */
    public void initProxyPath(String path)
    {
        this.restletProxy.setPath(path);
    }

    /**
     * Initialize the data proxy for logs grid then set data loader and data store. Finally load first request.
     */
    public void initPagingProxy()
    {
        loader = new BasePagingLoader<PagingLoadResult<AuditModelData>>(restletProxy, AuditReaderSingleton
                .getInstance());
        store = new ListStore<AuditModelData>(loader);
    }

    /**
     * Refresh grid data depending on constraints given in parameter.
     * 
     * @param constraints
     */
    public void refreshData(List<IUrlParameter> constraints)
    {
        restletProxy.clearParameters();
        restletProxy.addParameters(constraints);

        this.reloadStore();
    }
}
