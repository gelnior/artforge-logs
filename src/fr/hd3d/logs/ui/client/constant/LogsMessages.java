package fr.hd3d.logs.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * 	'/home/frank.rousseau/workspace/Logs/war/WEB-INF/classes/fr/hd3d/logs/ui/client/constant/LogsMessages.properties'.
 */
public interface LogsMessages extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "LogsMessages".
   * 
   * @return translated "LogsMessages"
   */
  @DefaultStringValue("LogsMessages")
  @Key("ClassName")
  String ClassName();
}
