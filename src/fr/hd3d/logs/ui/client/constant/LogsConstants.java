package fr.hd3d.logs.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * 	'/home/frank.rousseau/workspace/Logs/war/WEB-INF/classes/fr/hd3d/logs/ui/client/constant/LogsConstants.properties'.
 */
public interface LogsConstants extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "ApplyFilter".
   * 
   * @return translated "ApplyFilter"
   */
  @DefaultStringValue("ApplyFilter")
  @Key("ApplyFilter")
  String ApplyFilter();

  /**
   * Translated "LogsConstants".
   * 
   * @return translated "LogsConstants"
   */
  @DefaultStringValue("LogsConstants")
  @Key("ClassName")
  String ClassName();

  /**
   * Translated "End Date:".
   * 
   * @return translated "End Date:"
   */
  @DefaultStringValue("End Date:")
  @Key("EndDate")
  String EndDate();

  /**
   * Translated "Logs".
   * 
   * @return translated "Logs"
   */
  @DefaultStringValue("Logs")
  @Key("Logs")
  String Logs();

  /**
   * Translated "Start Date:".
   * 
   * @return translated "Start Date:"
   */
  @DefaultStringValue("Start Date:")
  @Key("StartDate")
  String StartDate();
}
