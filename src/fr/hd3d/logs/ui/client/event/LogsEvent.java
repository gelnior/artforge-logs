package fr.hd3d.logs.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Logs application events.
 * 
 * @author HD3D
 */
public class LogsEvent
{
    public static final EventType FILTER_CLICKED = new EventType();
}
