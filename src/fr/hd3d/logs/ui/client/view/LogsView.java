package fr.hd3d.logs.ui.client.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.binding.FieldBinding;
import com.extjs.gxt.ui.client.data.BasePagingLoadConfig;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Viewport;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.Time;
import com.extjs.gxt.ui.client.widget.form.TimeField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.logs.ui.client.config.LogsConfig;
import fr.hd3d.logs.ui.client.constant.LogsConstants;
import fr.hd3d.logs.ui.client.constant.LogsMessages;
import fr.hd3d.logs.ui.client.controller.LogsController;
import fr.hd3d.logs.ui.client.event.LogsEvent;
import fr.hd3d.logs.ui.client.model.LogsModel;
import fr.hd3d.logs.ui.client.model.modeldata.ConstraintForm;


/**
 * Main Logs View shows a grid displaying log data and a toolbar for filtering the grid data.
 * 
 * @author HD3D
 */
public class LogsView extends MainView implements ILogsView
{
    /** Constant strings to display : dialog messages, button label... */
    public static LogsConstants CONSTANTS = GWT.create(LogsConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static LogsMessages MESSAGES = GWT.create(LogsMessages.class);

    /** Model handling grid data. */
    private final LogsModel model = new LogsModel();

    /** Panel containing the logs grid. */
    private final ContentPanel mainPanel = new ContentPanel();
    /** Button bar containing date filters. */
    private final ToolBar buttonBar = new ToolBar();

    /** Grid to display and manipulates groups */
    private Grid<AuditModelData> grid;
    /** Column configuration needed by the grid to set columns. */
    private final List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

    /** Start date field included in the filter form. */
    private final DateField startDate = new DateField();
    /** Start hour field included in the filter form. */
    private final TimeField startHour = new TimeField();
    /** End date field included in the filter form. */
    private final DateField endDate = new DateField();
    /** End hour field included in the filter form. */
    private final TimeField endHour = new TimeField();
    /** Button used for submitting filter form. */
    private final ToolBarButton filterButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), CONSTANTS.ApplyFilter(),
            LogsEvent.FILTER_CLICKED);

    /**
     * Default constructor. Set application name, for start panel.
     */
    public LogsView()
    {
        super(CONSTANTS.Logs());
    }

    /**
     * Initializes controller and model.
     */
    @Override
    public void init()
    {
        LogsController mainController = new LogsController(model, this);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(mainController);

        ServicesField.initFields();
        ServicesPath.initPath();
        ServicesModelType.initModelTypes();
        PermissionUtil.setDebugOn();
    }

    /**
     * Display error message depending on error code passed in parameter.
     * 
     * @param error
     *            The code of the error to display.
     */
    @Override
    public void displayError(Integer error)
    {
        super.displayError(error);

        switch (error)
        {
            default:
                ;
        }
    }

    /** Initialize the UI widgets and display it in the navigator. */
    public void initWidgets()
    {
        this.setGridColumns();
        this.setStyle();
        this.setToolbar();
        this.setPagingToolBar();
        this.setBinders();

        BorderLayoutData centerData = new BorderLayoutData(LayoutRegion.CENTER);
        centerData.setMargins(new Margins(5, 5, 5, 5));
        this.mainPanel.add(this.grid, centerData);

        Viewport viewport = new Viewport();
        viewport.setLayout(new FitLayout());
        viewport.add(mainPanel);

        RootPanel.get().add(viewport);
    }

    /**
     * Auto select fields.
     */
    public void autoSelectFilterFields()
    {
        DateWrapper date = new DateWrapper();
        date = date.clearTime();

        ConstraintForm form = this.model.getConstraintForm();

        // this.startDate.setValue(date.asDate());
        // form.setStartDate(date.asDate());
        // this.endDate.setValue(date.addDays(1).asDate());
        // form.setEndDate(date.addDays(1).asDate());
        this.startHour.setValue(this.startHour.getStore().getAt(0));
        form.setStartHour(this.startHour.getStore().getAt(0));
        this.endHour.setValue(this.endHour.getStore().getAt(0));
        form.setEndHour(this.endHour.getStore().getAt(0));
    }

    /** Sets columns ID and Name into the grid, based on Audit Model Data type. */
    private void setGridColumns()
    {
        ColumnConfig columnDate = this.addColumnToConfig(AuditModelData.DATE_MS_FIELD,
                ServicesField.getHumanName(AuditModelData.DATE_FIELD), 110, false);
        this.addColumnToConfig(AuditModelData.OPERATION_FIELD,
                ServicesField.getHumanName(AuditModelData.OPERATION_FIELD), 80, false);
        this.addColumnToConfig(AuditModelData.ENTITY_ID_FIELD,
                ServicesField.getHumanName(AuditModelData.ENTITY_ID_FIELD), 80, false);
        this.addColumnToConfig(AuditModelData.ENTITY_NAME_FIELD,
                ServicesField.getHumanName(AuditModelData.ENTITY_NAME_FIELD), 100, true);
        this.addColumnToConfig(AuditModelData.FIRST_NAME_FIELD,
                ServicesField.getHumanName(AuditModelData.FIRST_NAME_FIELD), 100, true);
        this.addColumnToConfig(AuditModelData.LAST_NAME_FIELD,
                ServicesField.getHumanName(AuditModelData.LAST_NAME_FIELD), 100, true);
        this.addColumnToConfig(AuditModelData.LOGIN_FIELD, ServicesField.getHumanName(AuditModelData.LOGIN_FIELD), 100,
                false);
        ColumnConfig columnModification = this.addColumnToConfig(AuditModelData.MODIFICATION_FIELD,
                ServicesField.getHumanName(AuditModelData.MODIFICATION_FIELD), 300, true);

        columnModification.setSortable(false);
        columnDate.setRenderer(new GridCellRenderer<AuditModelData>() {
            public Object render(AuditModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                    ListStore<AuditModelData> store, Grid<AuditModelData> grid)
            {
                Date date = new Date(model.getDateMs());
                return DateFormat.FRENCH_DATE_TIME.format(date);
            }
        });

        final ColumnModel cm = new ColumnModel(configs);

        this.grid = new Grid<AuditModelData>(this.model.getStore(), cm);
        this.grid.setAutoExpandColumn(AuditModelData.MODIFICATION_FIELD);
        this.grid.setAutoExpandMax(1000);
        this.grid.addListener(Events.SortChange, new Listener<GridEvent<AuditModelData>>() {
            public void handleEvent(GridEvent<AuditModelData> be)
            {
                BasePagingLoadConfig config = new BasePagingLoadConfig(0, LogsConfig.PAGING_OFFSET);
                config.setSortInfo(be.getSortInfo());

                model.getLoader().load(config);
            }
        });
    }

    /** Set grid title, size and border style. */
    private void setStyle()
    {
        this.mainPanel.setHeaderVisible(false);

        this.grid.setLoadMask(true);
        this.grid.setBorders(true);

        this.mainPanel.setLayout(new BorderLayout());
    }

    /**
     * Set binders between filter form and application model.
     */
    private void setBinders()
    {
        FieldBinding startHourBinder = new FieldBinding(startHour, ConstraintForm.START_HOUR_FIELD);
        startHourBinder.bind(this.model.getConstraintForm());
        FieldBinding endHourBinder = new FieldBinding(endHour, ConstraintForm.END_HOUR_FIELD);
        endHourBinder.bind(this.model.getConstraintForm());
        FieldBinding startDateBinder = new FieldBinding(startDate, ConstraintForm.START_DATE_FIELD);
        startDateBinder.bind(this.model.getConstraintForm());
        FieldBinding endDateBinder = new FieldBinding(endDate, ConstraintForm.END_DATE_FIELD);
        endDateBinder.bind(this.model.getConstraintForm());
    }

    /**
     * Logs tool bar contains date picker for filtering on logs start date and logs end date, idem for start time and
     * end time.
     */
    private void setToolbar()
    {
        startHour.setWidth(60);
        startHour.setFormat(DateFormat.TIME);
        startHour.setTriggerAction(TriggerAction.ALL);
        startHour.setEditable(false);
        startDate.setWidth(85);
        startDate.getPropertyEditor().setFormat(DateFormat.DATE);
        startDate.setValue(new Date());

        endHour.setWidth(60);
        endHour.setFormat(DateFormat.TIME);
        endHour.setTriggerAction(TriggerAction.ALL);
        endHour.setEditable(false);
        endDate.setWidth(85);
        endDate.getPropertyEditor().setFormat(DateFormat.DATE);

        buttonBar.add(new LabelToolItem(CONSTANTS.StartDate()));
        buttonBar.add(startDate);
        buttonBar.add(startHour);
        buttonBar.add(new SeparatorToolItem());
        buttonBar.add(new LabelToolItem(CONSTANTS.EndDate()));
        buttonBar.add(endDate);
        buttonBar.add(endHour);

        buttonBar.add(new SeparatorToolItem());
        buttonBar.add(filterButton);

        buttonBar.setStyleAttribute("padding", "5px");
        this.mainPanel.setTopComponent(this.buttonBar);
    }

    /**
     * Sets the paging tool bar on the log data grid.
     */
    private void setPagingToolBar()
    {
        final PagingToolBar toolBar = new PagingToolBar(LogsConfig.PAGING_OFFSET);
        toolBar.bind(this.model.getLoader());
        toolBar.setStyleAttribute("padding", "5px");

        this.mainPanel.setBottomComponent(toolBar);
    }

    /**
     * Add a column to the column configuration object needed by the grid.
     * 
     * @param name
     *            Column identifier
     * @param Header
     *            Column header that is displayed
     * @param width
     *            Column default width
     * @param resizable
     *            If true, column is resizable.
     */
    private ColumnConfig addColumnToConfig(String name, String header, int width, boolean resizable)
    {
        final ColumnConfig column = new ColumnConfig();
        column.setId(name);
        column.setHeader(header);
        column.setWidth(width);
        column.setResizable(resizable);

        configs.add(column);

        return column;
    }

    /**
     * Hide loading mask for logs grid.
     */
    public void unmaskGrid()
    {
        if (this.grid != null)
        {
            this.grid.unmask();
        }
    }

    /**
     * Convert a date to millisecond.
     * 
     * @see fr.hd3d.logs.ui.client.view.ILogsView#convertDateToLong(java.util.Date,
     *      com.extjs.gxt.ui.client.widget.form.Time)
     */
    public Long convertDateToLong(Date dateToConvert, Time hour)
    {
        DateWrapper dateWrapper = new DateWrapper(dateToConvert);
        dateWrapper = dateWrapper.addHours(hour.getHour());
        dateWrapper = dateWrapper.addMinutes(hour.getMinutes());
        Date date = dateWrapper.asDate();

        Long dateLong = new Long(date.getTime());

        return dateLong;
    }

    public void setStartDate(Date startDate)
    {
        ConstraintForm form = this.model.getConstraintForm();
        this.startDate.setValue(startDate);
        form.setStartDate(startDate);
    }

    public void setEndDate(Date endDate)
    {
        ConstraintForm form = this.model.getConstraintForm();
        this.endDate.setValue(endDate);
        form.setEndDate(endDate);
    }

}
