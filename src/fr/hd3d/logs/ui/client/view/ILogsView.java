package fr.hd3d.logs.ui.client.view;

import java.util.Date;

import com.extjs.gxt.ui.client.widget.form.Time;

import fr.hd3d.common.ui.client.widget.mainview.IMainView;


/**
 * This interface represents the main application view.
 * 
 * @author HD3D
 */
public interface ILogsView extends IMainView
{
    /**
     * Build all widgets before displaying.
     */
    public void initWidgets();

    public void autoSelectFilterFields();

    public void unmaskGrid();

    public Long convertDateToLong(Date startDate, Time startHour);

    public void setStartDate(Date date);

    public void setEndDate(Date date);
}
