package fr.hd3d.logs.ui.client.config;



/**
 * Constants needed by the Logs application.
 * 
 * @author HD3D
 */
public class LogsConfig
{

    /** Default offset for paging grid. */
    public static int PAGING_OFFSET = 50;
}
