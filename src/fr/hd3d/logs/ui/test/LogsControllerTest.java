package fr.hd3d.logs.ui.test;

import org.junit.Test;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.logs.ui.client.controller.LogsController;
import fr.hd3d.logs.ui.client.event.LogsEvent;
import fr.hd3d.logs.ui.client.model.AuditReaderSingleton;
import fr.hd3d.logs.ui.client.model.LogsModel;


public class LogsControllerTest extends UITestCase
{
    private final LogsModel model = new LogsModel();
    private final LogsViewMock view = new LogsViewMock();
    private final LogsController controller = new LogsController(model, view);

    @Override
    public void setUp()
    {
        super.setUp();

        AuditReaderSingleton.setInstanceAsMock(new PagingReaderMock());

        EventDispatcher.get().getControllers().clear();
        EventDispatcher.get().addController(controller);
    }

    @Test
    public void testOnError()
    {
        this.view.setGridMasked(true);

        EventDispatcher.forwardEvent(CommonEvents.ERROR);
        assertFalse(this.view.isGridMasked());
    }

    @Test
    public void testOnDataRetrieved()
    {
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        assertTrue(this.view.isWidgetInitialized());
        assertTrue(this.view.isLoadingHidden());
        assertTrue(this.view.isFilterSet());
    }

    @Test
    public void testOnFilterClicked()
    {
        EventDispatcher.forwardEvent(LogsEvent.FILTER_CLICKED);
    }
}
