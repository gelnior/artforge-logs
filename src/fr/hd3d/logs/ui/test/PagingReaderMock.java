package fr.hd3d.logs.ui.test;

import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dPagingJsonReaderMock;
import fr.hd3d.logs.ui.client.model.modeldata.AuditModelType;


public class PagingReaderMock extends Hd3dPagingJsonReaderMock<AuditModelData> implements IPagingReader<AuditModelData>
{

    public PagingReaderMock()
    {
        super(new AuditModelType());
    }

    @Override
    public AuditModelData newModelInstance()
    {
        return new AuditModelData();
    }

}
