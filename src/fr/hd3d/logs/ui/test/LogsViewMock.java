package fr.hd3d.logs.ui.test;

import java.util.Date;

import com.extjs.gxt.ui.client.widget.form.Time;

import fr.hd3d.logs.ui.client.view.ILogsView;


public class LogsViewMock implements ILogsView
{
    private boolean isGridMasked = false;
    private boolean isWidgetInitialized = false;
    private boolean isLoadingHidden = false;
    private boolean isFilterSet = false;

    public boolean isFilterSet()
    {
        return isFilterSet;
    }

    public void setFilterSet(boolean isFilterSet)
    {
        this.isFilterSet = isFilterSet;
    }

    public boolean isWidgetInitialized()
    {
        return isWidgetInitialized;
    }

    public boolean isLoadingHidden()
    {
        return isLoadingHidden;
    }

    public boolean isGridMasked()
    {
        return isGridMasked;
    }

    public void setGridMasked(boolean isGridMasked)
    {
        this.isGridMasked = isGridMasked;
    }

    public void autoSelectFilterFields()
    {
        this.isFilterSet = true;
    }

    public void initWidgets()
    {
        this.isWidgetInitialized = true;
    }

    public void unmaskGrid()
    {
        this.isGridMasked = false;
    }

    public void displayError(Integer error)
    {
        // TODO Auto-generated method stub

    }

    public void hideStartPanel()
    {
        this.isLoadingHidden = true;
    }

    public void showStartPanel()
    {
        this.isLoadingHidden = false;
    }

    public Long convertDateToLong(Date startDate, Time startHour)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void displayError(Integer error, String stack)
    {
        // TODO Auto-generated method stub

    }

    public void displayError(Integer intValue, String userMsg, String stack)
    {
        // TODO Auto-generated method stub

    }

    public void setStartDate(Long valueOf)
    {
        // TODO Auto-generated method stub

    }

    public void setEndDate(Long valueOf)
    {
        // TODO Auto-generated method stub

    }

    public void setStartDate(Date date)
    {
        // TODO Auto-generated method stub

    }

    public void setEndDate(Date date)
    {
        // TODO Auto-generated method stub

    }

	@Override
	public void showScriptRunningPanel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hideScriptRunningPanel() {
		// TODO Auto-generated method stub
		
	}

}
